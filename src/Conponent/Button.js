import React from 'react'
import { Rect } from 'react-konva'

class Button extends React.Component{
  constructor(props){
    super(props)
  }
  
  render(){
    var data = this.props.data
    var x = Math.floor(data.x - data.width/2)
    var y = Math.floor(data.y - data.height/2)
    return(
      <Rect x={x} y={y} width={data.width} height={data.height} onClick={data.onClick} fill={data.color}/>
    )
  }
}

export default Button