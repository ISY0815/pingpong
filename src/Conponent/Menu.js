import React from 'react'
import { Layer, Text } from 'react-konva'
import Button from './Button';

class Menu extends React.Component{
  constructor(props){
    super(props)
    this.state = {
      button: {
        start: {
          x: window.innerWidth/2,
          y: 700,
          width: 350,
          height: 75,
          color: "#feb236"
          // onClick: 
        }
      }
    }
  }
  
  render(){
    return(
      <Layer>
        <Text />
        <Button key={"start"} data={this.state.button.start}/>
      </Layer>
    )
  }
}

export default Menu