import React from 'react'
import ReactDOM from 'react-dom'
import { Stage, Layer, Rect, Text } from 'react-konva'
import './index.css'
import Menu from './Conponent/Menu';
import Gamezone from './Conponent/Gamezone';

class App extends React.Component{
  constructor(props){
    super(props)
  }

  render(){
    return(
      <Stage width={window.innerWidth} height={window.innerHeight}>
        <Menu/>
        <Gamezone/>
      </Stage>
    )
  }
}

ReactDOM.render(<App/>, document.getElementById('root'))